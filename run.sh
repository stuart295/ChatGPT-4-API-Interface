#!/bin/bash

# Activate the Python virtual environment
source ./venv/bin/activate

# Start the Flask app
python app.py
